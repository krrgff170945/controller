package com.app.Controller;

import com.app.Domain.Human;
import com.app.Receiver.Receiver;
import com.app.Sender.Sender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class Controller
{
    @Autowired
    Receiver receiver;

    @Autowired
    Sender sender;

    @GetMapping("/human")
    private void receiveRequest()
    {
        receiver.receive("r2c ");
    }

    @PostMapping("/human")
    private int saveStudent(@RequestBody Human human)
    {
        sender.send(""+
                human.getId()+"-"+
                human.getCode()+"-"+
                human.getName()+"-"+
                human.getFruit());
        return 0;
    }
}
